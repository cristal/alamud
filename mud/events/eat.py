# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class EatEvent(Event2):
    NAME = "eat"

    def perform(self):
        if not self.object.has_prop("eatable"):
            self.fail()
            return self.inform("eat.failed")
        self.inform("eat")

class EatWithEvent(Event3):
	NAME = "eat-with"

	def perform(self):
		if not self.object.has_prop("eatable_with"):
			self.fail()
			return self.inform("eat-with.failed")
		if not self.object2.has_prop("drinkable"):
			self.fail()
			return self.inform("eat-with.failed")
		self.inform("eat-with")
